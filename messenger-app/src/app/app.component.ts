import {Component} from '@angular/core';
import {User} from './model/user';
import {Message} from './model/message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'messenger-app';

  users = [
    new User('John', [new Message(true, 'Hi!', new Date())]),
    new User('Igor', [new Message(true, 'Hello!', new Date())]),
    new User('Morten', [new Message(true, 'Good morning!', new Date())])
  ];
  selectedUser;

  assignUser(user: User) {
    this.selectedUser = user;
    console.log(this.selectedUser);
  }
}
