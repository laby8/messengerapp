import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../model/user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  @Input() users: User[];
  @Output() userSelection = new EventEmitter();
  selectedUser;

  constructor() {
  }

  ngOnInit(): void {
  }


  selectUser() {
    this.userSelection.emit(this.selectedUser[0]);
  }
}
