import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../model/user';
import {Message} from '../../model/message';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss']
})
export class ChatWindowComponent implements OnInit {
  @Input() user: User = {} as User;
  messageText = '';

  constructor() {
  }

  ngOnInit(): void {
  }

  sendMessage() {
    this.user.messages.push(new Message(false, this.messageText, new Date()));
    this.messageText = '';
  }

}
