export class Message {
  sentByAnotherUser: boolean;
  content: string;
  timestamp: Date;

  constructor(sentByAnotherUser: boolean, content: string, timestamp: Date) {
    this.sentByAnotherUser = sentByAnotherUser;
    this.content = content;
    this.timestamp = timestamp;
  }
}
